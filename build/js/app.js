'use strict';


document.addEventListener("DOMContentLoaded", function() {

    // SVG IE11 support
    svg4everybody();
    
    // Mask input
    $("input[name='phone']").mask("+9(999) 999-9999");

    $('.card-num').mask("9999-9999-9999-9999");
    $('.card-date').mask("99/99");
    $('.card-cvv').mask("999");

    // Nav
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('.nav-toggle').toggleClass('active');
        $('body').toggleClass('nav-open');

        $('.nav__second').removeClass('active');
        $('.nav__cat').removeClass('active');
        $('.nav-cat__second').removeClass('active');
    });


    $('.nav-parent').on('click', function(e){
        e.preventDefault();
        let child = $(this).attr('href');
        $(child).addClass('active');
    });

    $('.nav-second-close').on('click', function(e){
        e.preventDefault();
        $(this).closest('.nav__second').removeClass('active');
    });

    $('.cat-toggle').on('click', function(e){
        e.preventDefault();
        $('.nav__cat').toggleClass('active');
    });

    $('.nav-cat-parent').on('click', function(e){
        e.preventDefault();
        let child = $(this).attr('href');
        $(child).addClass('active');
    });

    $('.nav-child-close').on('click', function(e){
        e.preventDefault();
        $(this).closest('.nav-cat__second').removeClass('active');
    });

    // cat nav
    $('.cat-nav-parent').on('click', function(e){
        e.preventDefault();
        let child = $(this).attr('href');
        $(this).closest('.cat-nav__main').hide();
        $(child).show();
    });


    // Modal 
    $('.btn-modal').fancybox({
        autoFocus: false,
    });

    // bookmark
    $('.bookmark-toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('added');
    });

    // products-bnr
    $('.products-bnr__close').on('click', function(e){
        e.preventDefault();
        $(this).closest('.products-bnr').hide();
    });


    let offer = new Swiper('.offer-slider', {
        loop: false,
        slidesPerView: 2,
        slidesPerColumn: 2,
        slidesPerGroup: 2,
        spaceBetween: 14,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            768: {
                spaceBetween: 30,
            },
        }
    });

    // filter

    $('.filter-button').on('click', function(e){
        e.preventDefault();
        $('.products__sidebar').toggleClass('open')
    });

    $('.filter__header--arrow').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('toggle');
        $(this).closest('.filter').find('.filter__content').slideToggle('fast')
    });

    $('.filter__group--arrow').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('toggle');
        $(this).closest('.filter__group').find('.filter__group--content').slideToggle('fast')
    });

    let $range = $(".filter-range");

    $range.ionRangeSlider({
        skin: "round"
    });


    $range.on("change", function () {
        let $inp = $(this);
        let v = $inp.prop("value");
        let from = $inp.data("from");
        let to = $inp.data("to");

        $('.price-from').val(from);
        $('.price-to').val(to);

    });


    // Select
    $('select').selectric({

    });

    $.fn.textInputBox = function (boxClass) {
        if (!boxClass) boxClass = 'input-field';
        $(this).find('.' + boxClass).each(function (index, element) {
            const $box = $(element);
            const $input = $box.find('input, textarea');
            if ($input.attr('required') && !$box.hasClass(`${boxClass}--required`)) {
                $box.addClass(`${boxClass}--required`);
            }
            if ($input.attr('placeholder')) {
                $box.attr('data-placeholder', $input.attr('placeholder'));
            }
            hasValue();
            $input.on('change', hasValue);
            $input.on('focus', function () {
                $box.addClass(`${boxClass}--focus`);
                hasValue();
            }).on('blur', function () {
                $box.removeClass(`${boxClass}--focus`);
                hasValue();
            });

            function hasValue() {
                const hasValueClass = `${boxClass}--has-value`;
                if ($input.val().length > 0) {
                    if (!$box.hasClass(hasValueClass)) $box.addClass(hasValueClass);
                } else {
                    if ($box.hasClass(hasValueClass)) $box.removeClass(hasValueClass);
                }
            }
        });
    }

    $('body').textInputBox();

    // Product gallery

    let gallery = new Swiper('.product-slider', {
        loop: true,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    });


    function numberInputBoxPlugin() {
        jQuery('.quantity input').wrap('<div class="quantity__input"></div>');
        jQuery('<div class="quantity__button quantity__button--up"></div>').insertAfter('.quantity__input');
        jQuery('<div class="quantity__button quantity__button--down"></div>').insertBefore('.quantity__input');

        jQuery('.quantity').each(function() {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity__button--up'),
                btnDown = spinner.find('.quantity__button--down'),
                min = input.attr('min'),
                max = input.attr('max');

            btnUp.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            btnDown.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            input.on('change',function(){
                if(+$(this).val() > max) {
                    $(this).val(max);
                }
            });

        });
    }

    function numberInputCart() {
        jQuery('.count input').wrap('<div class="count__input"></div>');
        jQuery('<div class="count__button count__button--up"></div>').insertAfter('.count__input');
        jQuery('<div class="count__button count__button--down"></div>').insertBefore('.count__input');

        jQuery('.count').each(function() {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.count__button--up'),
                btnDown = spinner.find('.count__button--down'),
                step = parseFloat(input.attr('step')),
                min = input.attr('min'),
                max = input.attr('max');

            btnUp.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + step;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            btnDown.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - step;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            input.on('change',function(){
                if(+$(this).val() > max) {
                    $(this).val(max);
                }
            });

        });
    }

    numberInputBoxPlugin();

    numberInputCart();

});


